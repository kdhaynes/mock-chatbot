# Mocking a Chatbot Server

Implement a chatbot server including unit tests while mocking the chatbot code using Mockito.

Implement tests that act as clients of a ChatbotServer, following the sequence of interactions:
- A client connects, SocketServer provides a Socket to ChatbotServer via the accept() method
- The client provides a string to the Socket
- The ChatbotServer receives the string and passes it to the Chatbot
- The Chatbot generates a response and returns it to the ChatbotServer
- The ChatbotServer provides the response to the client via the Socket
- The client may optionally send more strings and receive more responses, one response per input string
- The client disconnects and the SocketServer goes back to waiting for another client

The ChatbotServer must gracefully handle exception from the Chatbot and from the networking code.



