package a2;

import static org.junit.Assert.*;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import static org.mockito.Mockito.when;
import static org.mockito.Matchers.*;

/**
 * Tests for the Chatbot Server.
 * 
 * @author Katherine Haynes
 */

@RunWith(MockitoJUnitRunner.class)
public class TestChatbotServer {

	@Mock
	public Chatbot mockChatbot;

	@Mock
	public ServerSocket mockServerSocket;

	@Mock
	public Socket mockSocket;

	public ChatbotServer myServer;

	@Before
	public void setUp() {
		myServer = new ChatbotServer(mockChatbot, mockServerSocket);
	}

	@Test
	public void testHappyPathOneLine() throws Exception {
		when(mockServerSocket.accept()).thenReturn(mockSocket);

		InputStream s = new ByteArrayInputStream("chatbot\n".getBytes());
		when(mockSocket.getInputStream()).thenReturn(s);

		OutputStream myOutputStream = new ByteArrayOutputStream();
		when(mockSocket.getOutputStream()).thenReturn(myOutputStream);

		String response = "HI";
		when(mockChatbot.getResponse(anyString())).thenReturn(response);
		myServer.handleOneClient();

		assertEquals("HI\n", myOutputStream.toString());
	}

	@Test
	public void testAIException() throws Exception {
		when(mockServerSocket.accept()).thenReturn(mockSocket);

		InputStream s = new ByteArrayInputStream("chatbot\n".getBytes());
		when(mockSocket.getInputStream()).thenReturn(s);

		OutputStream myOutputStream = new ByteArrayOutputStream();
		when(mockSocket.getOutputStream()).thenReturn(myOutputStream);

		when(mockChatbot.getResponse(anyString())).thenThrow(new AIException("Bad One Line"));
		myServer.handleOneClient();

		String hello = "Got AI Exception: Bad One Line\n";
		assertEquals(hello, myOutputStream.toString());
	}

	@Test
	public void testHappyPathTwoLines() throws Exception {
		when(mockServerSocket.accept()).thenReturn(mockSocket);

		String buffer = "chatbot\nmore\n";
		InputStream s = new ByteArrayInputStream(buffer.getBytes());
		when(mockSocket.getInputStream()).thenReturn(s);

		OutputStream myOutputStream = new ByteArrayOutputStream();
		when(mockSocket.getOutputStream()).thenReturn(myOutputStream);

		String response1 = "HI";
		String response2 = "HELLO";
		String netresponse = "HI\nHELLO\n";
		when(mockChatbot.getResponse(anyString())).thenReturn(response1).thenReturn(response2);

		myServer.handleOneClient();

		assertEquals(netresponse, myOutputStream.toString());
	}

	@Test
	public void testHappyAIExceptionHappy() throws Exception {
		when(mockServerSocket.accept()).thenReturn(mockSocket);

		String buffer = "chatbot\nmore\nnow\n";
		InputStream s = new ByteArrayInputStream(buffer.getBytes());
		when(mockSocket.getInputStream()).thenReturn(s);

		OutputStream myOutputStream = new ByteArrayOutputStream();
		when(mockSocket.getOutputStream()).thenReturn(myOutputStream);

		String response1 = "HI";
		String response2 = "HELLO";
		String netresponse = "HI\nGot AI Exception: Bad Middle Line\nHELLO\n";
		when(mockChatbot.getResponse(anyString()))
				.thenReturn(response1)
				.thenThrow(new AIException("Bad Middle Line"))
				.thenReturn(response2);

		myServer.handleOneClient();

		assertEquals(netresponse, myOutputStream.toString());
	}

	@Test
	public void testInStreamBadThenTwoLines() throws Exception {
		String buffer = "my question \n ask again \n";
		String response1 = "chatbot confused";
		String response2 = "chatbot answered";
		InputStream s = new ByteArrayInputStream(buffer.getBytes());
		OutputStream myOutputStream = new ByteArrayOutputStream();

		when(mockServerSocket.accept()).thenReturn(mockSocket);
		when(mockSocket.getInputStream()).thenReturn(s);
		when(mockSocket.getOutputStream()).thenThrow(new IOException()).thenReturn(myOutputStream);
		String try1 = "my question ";
		String try2 = " ask again ";
		when(mockChatbot.getResponse(try1)).thenReturn(response1);
		when(mockChatbot.getResponse(try2)).thenReturn(response2);

		myServer.handleOneClient();
		myServer.handleOneClient();

		String test = "chatbot confused\nchatbot answered\n";
		assertEquals(test, myOutputStream.toString());
	}

	@Test
	public void testSocketBadThenAIThenHappy() throws Exception {
		String buffer = "chatbot\nmore\n";
		String response1 = "chatbot response";
		InputStream s = new ByteArrayInputStream(buffer.getBytes());
		OutputStream myOutputStream = new ByteArrayOutputStream();

		when(mockServerSocket.accept()).thenThrow(new IOException()).thenReturn(mockSocket);
		when(mockSocket.getInputStream()).thenReturn(s);
		when(mockSocket.getOutputStream()).thenReturn(myOutputStream);
		when(mockChatbot.getResponse(anyString())).thenThrow(new AIException("Bad IO And Response"))
				.thenReturn(response1);

		myServer.handleOneClient();
		myServer.handleOneClient();

		String test = "Got AI Exception: Bad IO And Response\nchatbot response\n";
		assertEquals(test, myOutputStream.toString());
	}
	
	@Test
	public void testHappyPathTwoClients() throws Exception {
		when(mockServerSocket.accept()).thenReturn(mockSocket);

		InputStream s = new ByteArrayInputStream("chatbot\n".getBytes());
		when(mockSocket.getInputStream()).thenReturn(s);

		OutputStream myOutputStream = new ByteArrayOutputStream();
		when(mockSocket.getOutputStream()).thenReturn(myOutputStream);

		String response = "HI";
		when(mockChatbot.getResponse(anyString())).thenReturn(response);
		myServer.handleOneClient();
		assertEquals("HI\n", myOutputStream.toString());
		
		myServer.handleOneClient();
		assertEquals("HI\n", myOutputStream.toString());
	}

}
